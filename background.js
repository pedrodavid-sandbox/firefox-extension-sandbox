"use strict"

const SHARE_TABS_ID = "share-tabs"

function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

browser.contextMenus.create(
  {
    id: SHARE_TABS_ID,
    title: browser.i18n.getMessage("contextMenuItemSelectionLogger"),
    contexts: [ "tab" ],
  },
  onCreated
)

function shareHighlightedTabs(tabs) {
  const links = tabs.map(({ title, url }) => `[${title}](${url})`)
      .join("\n")
  navigator.clipboard.writeText(links)
}

browser.contextMenus.onClicked.addListener((info, tab) => {
  switch (info.menuItemId) {
    case SHARE_TABS_ID:
      // We only allow this for the current window because other windows
      // will always have 1 tab highlighted and we don't actually want that tab
      // In the future, we may want to compare the highlighted tabs with the current one
      // (possibly second parameter of this lambda).
      browser.tabs.query({ currentWindow: true, highlighted: true })
        .then(shareHighlightedTabs)
      break;
  }
});